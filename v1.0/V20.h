#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_V20.h"
#include "Convert.h"
#include <QWidget>

namespace Ui {
	class V20;
}
class V20 : public QMainWindow
{
	Q_OBJECT

public:
	V20(QWidget *parent = Q_NULLPTR);
	~V20();

	Convert cv;

private:
	Ui::V10Class *ui;

public slots:
	void start_cv();
};
