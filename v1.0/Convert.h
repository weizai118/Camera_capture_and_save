#pragma once

#include <QThread>
#include <qdebug.h>

class Convert : public QThread
{
	Q_OBJECT

public:
	Convert();
	~Convert();

	void StartConvert();
	int input_w;
	int input_h;
	char *out_file_name;
	int framenumber;

protected:
	void run();
};
